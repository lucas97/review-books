(ns app.ui
  (:require [app.application :refer [SPA]]
            [com.fulcrologic.fulcro.application :as app]
            [com.fulcrologic.fulcro.components :as comp :refer [defsc]]
            [com.fulcrologic.fulcro.dom :as dom]))

(defsc Book [this {:book/keys [id title rank comment]}]
  {:query [:book/id :book/title :book/rank :book/comment]
   :ident :book/id
   :initial-state (fn [{:keys [id title rank comment]}]
                    {:book/id id
                     :book/title title
                     :book/rank rank
                     :book/comment comment})}
  (dom/div (dom/h3 title)
    (dom/p (str "Star: " rank))
    (dom/p (str "Comment: " comment))))

(def ui-book (comp/factory Book {:keyfn :book/id}))

(defsc Root [this {:keys [books]}]
  {:query [{:books (comp/get-query Book)}]
   :initial-state (fn [_]
                    {:books [(comp/get-initial-state Book {:id 1 :title "Dune"
                                                          :rank 9 :comment "Pretty darn good!"})
                            (comp/get-initial-state Book {:id 2 :title "Moneyball"
                                                          :rank "?" :comment "In the list!"})]})}
  (dom/div
    (dom/h2 "Let's go!")
    (map ui-book books)))


(comment
  (reset! (::app/state-atom SPA) {})
  (app/schedule-render! SPA {:force-root? true})
  (app/mount! SPA Root "app")
  (comp/refresh-dynamic-queries! SPA)
  (app/current-state SPA)
  )
