(ns app.client
  (:require [app.application :refer [SPA] ]
            [app.ui :as ui]
            [com.fulcrologic.fulcro.application :as app]
            [com.fulcrologic.fulcro.components :as comp]))

(defn ^:export init []
  (app/mount! SPA ui/Root "app")
  (js/console.log "Ready, set and go!"))

(defn ^:export refresh []
  "remounting will cause ui to refresh"
  (app/mount! SPA ui/Root "app")
  (comp/refresh-dynamic-queries! SPA)
  (js/console.log "Sucessfull refresh!"))
