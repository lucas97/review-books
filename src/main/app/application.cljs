(ns app.application
  (:require [com.fulcrologic.fulcro.application :as app]
            [com.fulcrologic.fulcro.react.version18 :refer [with-react18]]
            ["react-dom/client" :as dom-client]))

(defonce SPA (-> (app/fulcro-app {})
               (with-react18)))
