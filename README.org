* A book review append
  The structure should be something like

  #+begin_src clojure
    {:book {1 {:book/id 'a-id
	       :book/name 'a-name
	       :book/rank 'a-ranking
	       :book/comment 'a-comment
	       }}}
  #+end_src
